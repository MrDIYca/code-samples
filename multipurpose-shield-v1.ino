
/*  ============================================================================

    MrDIY - Mutipurpose Shield

    Sample code for the sensors

    Example output with alerts:
    
    -> BTN#1 changes one LED colours
    -> BTN#2 plays tones
    -> PIR changes 2 LED colours to Red when it detects motion
    -> Temp & Humidity are saved to EEPROM every 60 seconds

    Do not forget to install the libraries.

  ============================================================================= */

#include <Wire.h>
#include <FastLED.h>
#include <EEPROM.h>
#include <SD.h>
#include <SPI.h>
#include <Adafruit_AHTX0.h>

// Define constants
#define PIN_BUTTON_1 27
#define PIN_BUTTON_2 26
#define PIN_PHOTO_RESISTOR 36
#define PIN_BUZZER 14
#define PIN_PIR 25
#define PIN_RADAR 34
#define PIN_SCL 22
#define PIN_SDA 21
#define PIN_THERMISTOR 39
#define PIN_RGB_LED 13
#define PIN_EEPROM_SCL 22
#define PIN_EEPROM_SDA 21
#define PIN_SSR_RELAY 12
#define PIN_HALL_EFFECT_1 33
#define PIN_HALL_EFFECT_2 32
#define PIN_SDCARD 5

#define AHT20_I2C_ADDR 0x38

#define LIGHT_SENSOR_THRESHOLD 1000
#define MELODY_LENGTH 4
#define EEPROM_RECORD_INTERVAL 60000 // 1 minute in milliseconds

static bool button1LastState = HIGH;
static bool button2LastState = HIGH;
static bool hallEffect1LastState = HIGH;
static bool hallEffect2LastState = HIGH;

bool isDark = false; // Flag to track dark/bright state

// Define the type of RGB LED strip and its data pin
#define NUM_LEDS 4
CRGB leds[NUM_LEDS];
uint8_t brightness = 10;     // Set brightness level (adjust as needed)

// Define the melody for the second button
#define NOTE_C4 261.63 // Middle C
#define NOTE_E4 329.63 // E above Middle C
#define NOTE_G4 392.00 // G above Middle C
#define NOTE_C5 523.25 // High C

int melody[] = {NOTE_C4, NOTE_E4, NOTE_G4, NOTE_C5};
int noteDurations[] = {4, 4, 4, 4}; // 4 means a quarter note

Adafruit_AHTX0 aht;

void connectRelay() {

  digitalWrite(PIN_SSR_RELAY, HIGH);
  Serial.println(" RELAY..................CLOSED");
}

void disconnectRelay() {

  digitalWrite(PIN_SSR_RELAY, LOW);
  Serial.println(" RELAY..................OPEN");
}

void changeLedColor(int ledNumber, CRGB color) {

  if (ledNumber >= 0 && ledNumber < NUM_LEDS) {
    
    leds[ledNumber] = color;
    FastLED.setBrightness(brightness );
    FastLED.show();
  }
}

void playMelody() {

  Serial.println(" BUZZER.................PLAYING");

  for (int i = 0; i < 4; i++) {
    int noteDuration = 1000 / noteDurations[i];
    tone(PIN_BUZZER, melody[i], noteDuration);
    delay(noteDuration + 50);
    noTone(PIN_BUZZER);
  }
}

void recordTemperatureHumidity() {

  sensors_event_t humidity, temp;
  aht.getEvent(&humidity, &temp);
  int thermistorValue = analogRead(PIN_THERMISTOR);

  // Calculate the EEPROM address based on the current record number
  //int recordNumber = EEPROM.read(0);
  //int address = 1 + recordNumber * sizeof(float) * 2;

  // Write temperature and humidity to EEPROM
  //EEPROM.put(address, temperature);
  //EEPROM.put(address + sizeof(float), humidity);

  // Increment the record number and write it back to EEPROM
  //recordNumber++;
  //EEPROM.write(0, recordNumber);

  Serial.print(" AHT TEMP...............");
  Serial.print(temp.temperature);
  Serial.println("°C");

  Serial.print(" AHT HUM................");
  Serial.print(humidity.relative_humidity);
  Serial.println("%");

  Serial.print(" THR....................");
  Serial.println(thermistorValue);
}

void listFiles(const char* dirname) {

  Serial.println(" SDCARD.......................");

  File root = SD.open(dirname);
  if (!root) {
    Serial.println("       failed to open directory");
    return;
  }

  while (File entry = root.openNextFile()) {
    Serial.print("       /");
    Serial.println(entry.name());
    entry.close();
  }
}

void setup() {

  Serial.begin(115200);

  Serial.println("-----------------------------------");
  Serial.println("    MrDIY Multipurpose Shield");
  Serial.println("-----------------------------------");

  pinMode(PIN_BUTTON_1, INPUT);
  pinMode(PIN_BUTTON_2, INPUT);
  pinMode(PIN_PHOTO_RESISTOR, INPUT);
  pinMode(PIN_PIR, INPUT);
  pinMode(PIN_RADAR, INPUT);
  pinMode(PIN_THERMISTOR, INPUT);
  pinMode(PIN_HALL_EFFECT_1, INPUT);
  pinMode(PIN_HALL_EFFECT_2, INPUT);

  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_SSR_RELAY, OUTPUT);

  FastLED.addLeds<NEOPIXEL, PIN_RGB_LED>(leds, NUM_LEDS);

  // Initialize LEDC
  ledcSetup(0, 5000, 8);  // LEDC channel 0, frequency 5000Hz, resolution 8-bit
  ledcAttachPin(PIN_BUZZER, 0);  // Attach LEDC to the buzzer pin

  aht.begin();

  if (SD.begin(PIN_SDCARD)) {
    listFiles("/");
  }

  // Initialize EEPROM record number
  if (EEPROM.read(0) == 255) {
    EEPROM.write(0, 0);
  }

  recordTemperatureHumidity();
}

void loop() {

  // Example: Read sensors and perform actions

  int button1State = digitalRead(PIN_BUTTON_1);
  int button2State = digitalRead(PIN_BUTTON_2);
  int photoResistorValue = analogRead(PIN_PHOTO_RESISTOR);
  int pirState = digitalRead(PIN_PIR);
  int radarState = digitalRead(PIN_RADAR);
  int thermistorValue = analogRead(PIN_THERMISTOR);
  int hallEffect1State = digitalRead(PIN_HALL_EFFECT_1);
  int hallEffect2State = digitalRead(PIN_HALL_EFFECT_2);

  // Perform actions based on sensor readings
  // ...

  // Example: Button 1 rotates through colors for the first LED

  if (button1State == HIGH && button1LastState == LOW) {
    static int colorIndex = 0;
    CRGB color = CHSV(colorIndex, 255, 255);
    changeLedColor(0, color);
    colorIndex = (colorIndex + 5) % 256; // Change color every iteration
    Serial.println(" BTN #1.................RELEASED");

  } else if (button1State == LOW && button1LastState == HIGH) {
    Serial.println(" BTN #1.................PRESSED");
  }
  button1LastState = button1State;

  // Example: Button 2 plays a melody on the buzzer

  if (button2State == HIGH && button2LastState == LOW) {
    Serial.println(" BTN #2.................RELEASED");
  } else if (button2State == LOW && button2LastState == HIGH) {
    Serial.println(" BTN #2.................PRESSED");
    playMelody();
  }
  button2LastState = button2State;


  // Example: PIR sensor turns LED 2 and 3 red when there is motion, black when there is no motion

  static bool pirLastState = LOW;
  if (pirState == HIGH && pirLastState == LOW) {
    changeLedColor(0, CRGB::Blue);
    changeLedColor(1, CRGB::Blue);
    changeLedColor(2, CRGB::Red);
    changeLedColor(3, CRGB::Green);
    Serial.println(" PIR....................MOTION");
  } else if (pirState == LOW && pirLastState == HIGH) {
    changeLedColor(0, CRGB::Black);
    changeLedColor(1, CRGB::Black);
    changeLedColor(2, CRGB::Black);
    changeLedColor(3, CRGB::Black);
    Serial.println(" PIR....................NO MOTION");
  }
  pirLastState = pirState;

  // Example: Check if the room is dark based on the light sensor reading, then turn on relay (60V, 500mA MAX)
  //  0 = lightest - 4095 darkest

  if (photoResistorValue > 3000 ) { // LIGHT_SENSOR_THRESHOLD
    if (!isDark) {
      isDark = true;
      connectRelay(); // Turn on the relay
      Serial.println(" LIGHT..................DARK");
    }
  } else {
    if (isDark) {
      isDark = false;
      disconnectRelay(); // Turn off the relay
      Serial.println(" LIGHT..................BRIGHT");
    }
  }


  // Example: Check Hall Effect 1 sensor
  if (hallEffect1State == LOW && hallEffect1LastState == HIGH) {
    Serial.println(" MAGNET #1..............DETECTED");
  } else if (hallEffect1State == HIGH && hallEffect1LastState == LOW) {
    Serial.println(" MAGNET #1..............GONE");
  }
  hallEffect1LastState = hallEffect1State;

  // Example: Check Hall Effect 2 sensor
  if (hallEffect2State == LOW && hallEffect2LastState == HIGH) {
    Serial.println(" MAGNET #2..............DETECTED");
  } else if (hallEffect2State == HIGH && hallEffect2LastState == LOW) {
    Serial.println(" MAGNET #2..............GONE");
  }
  hallEffect2LastState = hallEffect2State;

  // Example: Record temperature and humidity to EEPROM every 1 minute

  static unsigned long lastRecordTime = 0;
  unsigned long currentTime = millis();

  if (currentTime - lastRecordTime >= 60000) { // 1 minute interval
    recordTemperatureHumidity();
    lastRecordTime = currentTime;
  }

}