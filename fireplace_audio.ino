#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
DFRobotDFPlayerMini myDFPlayer;

void setup()
{
  mySoftwareSerial.begin(9600);
  Serial.begin(115200);

  Serial.println();
  Serial.println(F("Initializing ..."));

  if (!myDFPlayer.begin(mySoftwareSerial)) { 
    Serial.println(F("Unable to begin: recheck the connection or insert an SD card "));
    while(true);
  }
  Serial.println(F("DFPlayer Mini online."));

  delay(30 * 1000);

  myDFPlayer.volume(30);  
  myDFPlayer.enableLoopAll();
}

void loop() {}
