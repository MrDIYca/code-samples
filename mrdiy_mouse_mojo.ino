/*  ============================================================================

    MrDIY - Mouse Mojo

    Source Code for the ESP32-S3 Seed Studio Board
 
    Get the ESP32-S3:
      - Amazon: https://amzn.to/3WDQaaS
      - Aliexpress - https://s.click.aliexpress.com/e/_DePQTtt

    Video instructions: https://youtu.be/j19073bcR58
    
  ============================================================================= */

#include "USB.h"
#include "USBHIDMouse.h"
USBHIDMouse Mouse;

void setup() {

  Mouse.begin();
  USB.begin();
  Mouse.move(100, 0, 0);
  delay(500);
  Mouse.move(0, 100, 0);
  delay(500);
  Mouse.move(-100, 0, 0);
  delay(500);
  Mouse.move(0,-100, 0);
}

void loop() {

   Mouse.move(11, 0, 0);
  delay(5);
  Mouse.move(-11, 0, 0);
  delay(60000);
}