
#include <ArduinoJson.h> /* version 5 */
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <LedMatrix.h>
#include <NTPClient.h>
#include <SPI.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>

/* -------------------------------------------------------------------------------

     This an rough ESP8266 implementation for the free-api-worldcup2022 API

     See my video on YouTube - https://youtu.be/q8BWjKHMd14

     More at https://github.com/raminmr/free-api-worldcup2022

     You must (1) register for a free API account by running this command line on your PC
     then (2) update your credentials below (see CHANGE ME)

        curl --location --request POST 'http://api.cup2022.ir/api/v1/user'
        --header 'Content-Type: application/json'
        --data-raw '{
        "name" : "Reza RAhiminia",
        "email": "r.rahiminia@gmail.com",
        "password": "******",
        "passwordConfirm" : "******"
        }'

  ------------------------------- CHANGE ME -----------------------------------*/

const char* wifiName  = "";
const char* wifiPass  = "";
String my_email       = "";
String my_password    = "";
int refresh_every     = 1 ; // in minutes

/* -----------------------------------------------------------------------------*/

const char *login = "http://api.cup2022.ir/api/v1/user/login";
const char *bymatch = "http://api.cup2022.ir/api/v1/bymatch/";

WiFiClient  wifiClient;
LedMatrix   ledMatrix = LedMatrix(4, 2); // 4 blocks on Pin 2
WiFiUDP     ntpUDP;
NTPClient   timeClient(ntpUDP, "pool.ntp.org");

void setup() {

  ledMatrix.init();
  ledMatrix.clear();
  ledMatrix.setIntensity(1);
  printString(F("Wi-Fi"));

  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();
  Serial.println(F("=================================================================="));
  Serial.println(F("       MrDIY"));
  Serial.println(F("=================================================================="));

  Serial.println( F("Connecting to Wi-Fi ...") );
  WiFi.begin(wifiName, wifiPass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
  }
  Serial.print(F("IP address: "));
  Serial.println(WiFi.localIP());

  timeClient.begin();
  timeClient.setTimeOffset(+10800); // Qatar +3 hours
}

void loop() {

  getScore();
  delay(refresh_every * 60 * 1000);
}

void getScore() {

  if ( getMatchId() == "1") {
    Serial.println("No live matches");
    printString(F("  -  "));
    return;
  }

  String token = getToken();
  if ( token == "x") return;

  Serial.print("Getting score: ");
  Serial.println(bymatch + getMatchId() );

  HTTPClient http;
  http.begin(wifiClient, bymatch + getMatchId() );
  http.addHeader("Authorization", "Bearer " + token);
  http.addHeader("Content-Type", "application/json");
  int httpCode = http.GET();
  String payload = http.getString();
  http.end();

  Serial.print("[");
  Serial.print(httpCode);
  Serial.print("] ");
  Serial.println(payload);

  if (httpCode == 200) {
    DynamicJsonBuffer jsonBuffer(1500);
    JsonObject& boot = jsonBuffer.parseObject(payload);

    if (boot.success()) {

      Serial.println(boot["data"][0]["home_team_en"].as<String>() + "(" + boot["data"][0]["home_score"].as<String>() + ") - " + boot["data"][0]["away_team_en"].as<String>() + "(" + boot["data"][0]["away_score"].as<String>() + ")");
      printString(  " " +
                    boot["data"][0]["home_score"].as<String>()
                    + "-"
                    + boot["data"][0]["away_score"].as<String>()
                 );
    }

  } else {
    Serial.println("[");
    Serial.println(httpCode);
    Serial.println("] Error");
  }
}

String getToken() {

  HTTPClient http;
  Serial.print(F("Getting token: "));
  Serial.println(login);

  http.begin(wifiClient, login);
  http.addHeader("Content-Type", "application/json");
  http.addHeader("Accept", "*/*");
  http.addHeader("Content-Length", "100");

  int httpCode = http.POST("{\"email\": \"" + my_email + "\",\"password\": \"" + my_password + "\"}");
  String payload = http.getString();
  http.end();

  Serial.print("[");
  Serial.print(httpCode);
  Serial.print("] ");
  Serial.println(payload);

  const size_t capacity = 300;
  DynamicJsonBuffer jsonBuffer(capacity);
  JsonObject& root = jsonBuffer.parseObject(payload);

  if (httpCode == 200 && root.success()) {
    return String(root["data"]["token"]);
  } else {
    Serial.print("Unable to get a token");
    return "x";
  }
}

void printString(String s) {

  ledMatrix.clear();
  ledMatrix.setText(s);
  ledMatrix.setTextAlignment(TEXT_ALIGN_LEFT);
  ledMatrix.drawText();
  ledMatrix.commit();
}

String getMatchId() {

  timeClient.update();
  time_t epochTime = timeClient.getEpochTime();
  struct tm *ptm = gmtime ((time_t *)&epochTime);
  int currentDay = ptm->tm_mday;
  int currentHour = timeClient.getHours();
  int current_game = 1;

  if ( currentDay == 20 && currentHour >= 19)  current_game = 3;

  if ( currentDay == 21 && currentHour >= 16)  current_game = 2;
  if ( currentDay == 21 && currentHour >= 19)  current_game = 1;
  if ( currentDay == 21 && currentHour >= 22)  current_game = 4;

  if ( currentDay == 22 && currentHour >= 13)  current_game = 5;
  if ( currentDay == 22 && currentHour >= 16)  current_game = 6;
  if ( currentDay == 22 && currentHour >= 19)  current_game = 7;
  if ( currentDay == 22 && currentHour >= 22)  current_game = 8;

  if ( currentDay == 23 && currentHour >= 13)  current_game = 9;
  if ( currentDay == 23 && currentHour >= 16)  current_game = 10;
  if ( currentDay == 23 && currentHour >= 19)  current_game = 11;
  if ( currentDay == 23 && currentHour >= 22)  current_game = 12;

  if ( currentDay == 24 && currentHour >= 13)  current_game = 16;
  if ( currentDay == 24 && currentHour >= 16)  current_game = 15;
  if ( currentDay == 24 && currentHour >= 19)  current_game = 14;
  if ( currentDay == 24 && currentHour >= 22)  current_game = 13;

  if ( currentDay == 25 && currentHour >= 13)  current_game = 17;
  if ( currentDay == 25 && currentHour >= 16)  current_game = 18;
  if ( currentDay == 25 && currentHour >= 19)  current_game = 19;
  if ( currentDay == 25 && currentHour >= 22)  current_game = 20;

  if ( currentDay == 26 && currentHour >= 13)  current_game = 21;
  if ( currentDay == 26 && currentHour >= 16)  current_game = 22;
  if ( currentDay == 26 && currentHour >= 19)  current_game = 23;
  if ( currentDay == 26 && currentHour >= 22)  current_game = 24;

  if ( currentDay == 27 && currentHour >= 13)  current_game = 25;
  if ( currentDay == 27 && currentHour >= 16)  current_game = 26;
  if ( currentDay == 27 && currentHour >= 19)  current_game = 27;
  if ( currentDay == 27 && currentHour >= 22)  current_game = 28;

  if ( currentDay == 28 && currentHour >= 13)  current_game = 29;
  if ( currentDay == 28 && currentHour >= 16)  current_game = 30;
  if ( currentDay == 28 && currentHour >= 19)  current_game = 31;
  if ( currentDay == 28 && currentHour >= 22)  current_game = 32;

  if ( currentDay == 29 && currentHour >= 18)  current_game = 35;
  if ( currentDay == 29 && currentHour >= 18)  current_game = 36; // same time, need to pick one
  if ( currentDay == 29 && currentHour >= 22)  current_game = 33;
  if ( currentDay == 29 && currentHour >= 22)  current_game = 34; // same time, need to pick one

  if ( currentDay == 30 && currentHour >= 18)  current_game = 37;
  if ( currentDay == 30 && currentHour >= 18)  current_game = 38; // same time, need to pick one
  if ( currentDay == 30 && currentHour >= 22)  current_game = 39;
  if ( currentDay == 30 && currentHour >= 22)  current_game = 40; // same time, need to pick one

  if ( currentDay == 1 && currentHour >= 18)  current_game = 33;
  if ( currentDay == 1 && currentHour >= 18)  current_game = 34; // same time, need to pick one
  if ( currentDay == 1 && currentHour >= 22)  current_game = 35;
  if ( currentDay == 1 && currentHour >= 22)  current_game = 36; // same time, need to pick one

  if ( currentDay == 2 && currentHour >= 18)  current_game = 37;
  if ( currentDay == 2 && currentHour >= 18)  current_game = 38; // same time, need to pick one
  if ( currentDay == 2 && currentHour >= 22)  current_game = 39;
  if ( currentDay == 2 && currentHour >= 22)  current_game = 40; // same time, need to pick one

  return String(current_game);

  /*
    Extracted from http://api.cup2022.ir/api/v1/match ( all games)

      {
         "_id":"629c9c8a5749c4077500ead4",
         "away_score":2,
         "away_scorers":[
            "null"
         ],
         "away_team_id":"4",
         "finished":"TRUE",
         "group":"A",
         "home_score":0,
         "home_scorers":[
            "null"
         ],
         "home_team_id":"3",
         "id":"1",
         "local_date":"11/21/2022 19:00",
         "matchday":"2",
         "persian_date":"1400-08-30 19:30",
         "stadium_id":"1",
         "time_elapsed":"finished",
         "type":"group",
         "home_team_fa":"سنگال",
         "away_team_fa":"هلند",
         "home_team_en":"Senegal",
         "away_team_en":"Nederlands",
         "home_flag":"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Flag_of_Senegal.svg/125px-Flag_of_Senegal.svg.png",
         "away_flag":"https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/125px-Flag_of_the_Netherlands.svg.png"
      }

  */
}
